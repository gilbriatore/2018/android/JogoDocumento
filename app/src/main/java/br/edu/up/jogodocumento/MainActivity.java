package br.edu.up.jogodocumento;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

  List<ImageView> tabuleiro;
  ImageView linha;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    GridLayout grid = (GridLayout) findViewById(R.id.tabuleiro);
    tabuleiro = (List<ImageView>) (List<?>) grid.getTouchables();
    linha = (ImageView) findViewById(R.id.linha);
  }

  public void onClickIniciarJogo(View v){
    for (ImageView casa : tabuleiro) {
      casa.setImageResource(R.drawable.btn_branco);
      casa.setClickable(true);
    }
    linha.setImageResource(R.drawable.fundo_transparente);
    jogadas = new char[9];
  }

  char jogadorDaVez = 'o';
  char[] jogadas = new char[9];
  int[][] sequencias = {
      {1,2,3}, {4,5,6}, {7,8,9}, {1,4,7},
      {2,5,8}, {3,6,9}, {1,5,9}, {3,5,7}
  };

  Map<Integer, Integer> linhas = new HashMap<>();
  {
    linhas.put(0, R.drawable.linha_horizontal_1);
    linhas.put(1, R.drawable.linha_horizontal_2);
    linhas.put(2, R.drawable.linha_horizontal_3);
    linhas.put(3, R.drawable.linha_vertical_1);
    linhas.put(4, R.drawable.linha_vertical_2);
    linhas.put(5, R.drawable.linha_vertical_3);
    linhas.put(6, R.drawable.linha_decrescente);
    linhas.put(7, R.drawable.linha_crescente);
  }

  public void onClickJogar(View v){

    ImageView btn = (ImageView) v;
    btn.setClickable(false);

    String tag = btn.getTag().toString();
    int posicao = Integer.parseInt(tag);
    jogadas[posicao -1] = jogadorDaVez;

    for (int index = 0; index < sequencias.length; index++) {

      int[] sequencia = sequencias[index];

      if (jogadas[sequencia[0] -1] == jogadorDaVez &&
          jogadas[sequencia[1] -1] == jogadorDaVez &&
          jogadas[sequencia[2] -1] == jogadorDaVez){

        for (ImageView casa : tabuleiro) {
          casa.setClickable(false);
        }

        int idImagem = linhas.get(index);
        linha.setImageResource(idImagem);

        break;
      }
    }

    if (jogadorDaVez == 'o'){
      btn.setImageResource(R.drawable.o_verde_hdpi);
      jogadorDaVez = 'x';
    } else {
      btn.setImageResource(R.drawable.x_preto_hdpi);
      jogadorDaVez = 'o';
    }
  }
}